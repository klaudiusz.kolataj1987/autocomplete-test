const { Router } = require('express');
const toRegex = require('to-regex');
const ListItem = require('../../models/ListItem');

const router = Router();

router.get('/:firstname/:page', async( req, res) => {
    const { firstname, page } = req.params;
    const pageVal = Number(page);

    try {
        const listItems = await ListItem.find(
            { 
                firstName: {$regex: firstname, $options: 'i'}
            }
        ).skip(pageVal).limit(7);
        const maxPageCount = await ListItem.find({ firstName: {$regex: firstname, $options: 'i'}}).countDocuments();
        if(!listItems) throw new Error('There is no listItems');
        const sorted = listItems.sort((a, b) => {
            return new Date(a.date).getTime() - new Date(b.date).getTime();
        });
        res.status(200).json({items: sorted, maxPageCount: Math.round(maxPageCount), page: pageVal < 7 ? 7 : pageVal});
    } catch (error) {
        res.status(500).json({
            message: error.message
        })
    }
});

router.post('/', async(req, res) => {
    const newListItem = new ListItem(req.body);
    try{
        const listItem = await newListItem.save();
        if(!listItem) throw new Error('Something went wrong when saving');
        res.status(200).json(listItem);
    } catch (error){
        res.status(500).json({
            message: error.message
        })
    }
});

module.exports = router;